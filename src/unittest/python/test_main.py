import json


def test(client):
    resp = client.post('/getPhrases',
                            headers={'Content-Type': 'application/json'},
                            data=json.dumps({'text': "This is a sample test"}))
    assert resp.data.decode("utf-8") == "[\"sample\",\"sample test\"]\n"
    assert resp.status_code == 200

