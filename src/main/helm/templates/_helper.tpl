{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "chart.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "imagePullSecret" }}
{{- if .Values.registry }}
{{- if and .Values.registry.password .Values.registry.username }}
{{- printf "{\"auths\": {\"%s\": {\"auth\": \"%s\"}}}" .Values.registry.host (printf "%s:%s" .Values.registry.username .Values.registry.password | b64enc) | b64enc }}
{{- end }}
{{- end }}
{{- end }}

{{/*
  Define the labels that should be applied to all resources in the chart
*/}}
{{- define "common.labels.standard" -}}
app.kubernetes.io/name: {{ include "chart.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/version: {{ .Chart.Version }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app.kubernetes.io/part-of: ku
helm.sh/chart: {{ .Chart.Name }}-{{ .Chart.Version }}
app: {{ include "chart.name" . }}
version: {{ .Chart.Version }}
{{- end -}}

{{/*
Labels to use on deploy.spec.selector.matchLabels and svc.spec.selector
*/}}
{{- define "common.labels.matchLabels" -}}
app.kubernetes.io/name: {{ include "chart.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Platform namespace
*/}}
{{- define "platform.namespace" -}}
{{- default "ku-platform" .Values.global.platformNamespace -}}
{{- end -}}

{{/*
ProcessingGroup's common part. If not set, trimPrefix ku- from chart.name
*/}}
{{- define "processingGroup.common" -}}
{{- default ( (include "chart.name" .) | trimPrefix "ku-" ) .Values.axon.processingGroupCommon -}}
{{- end -}}