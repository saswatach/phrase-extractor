import os

from flask import Flask, render_template, jsonify,request
import nltk
from collections import OrderedDict


app = Flask(__name__)

@app.route("/getPhrases", methods=['POST'])
def output():
    data = request.get_json()
    text = data['text']
    phrases = []
    try:

        # Used when tokenizing words
        sentence_re = r'''(?x)          # set flag to allow verbose regexps
                (?:[A-Z]\.)+        # abbreviations, e.g. U.S.A.
              | \w+(?:-\w+)*        # words with optional internal hyphens
              | \$?\d+(?:\.\d+)?%?  # currency and percentages, e.g. $12.40, 82%
              | \.\.\.              # ellipsis
              | [][.,;"'?():_`-]    # these are separate tokens; includes ], [
            '''

        lemmatizer = nltk.WordNetLemmatizer()
        stemmer = nltk.stem.porter.PorterStemmer()

        #Taken from Su Nam Kim Paper...
        grammar = r"""
            NBAR:
                {<NN.*|JJ>*<NN.*>}  # Nouns and Adjectives, terminated with Nouns
                
            NP:
                {<NBAR>}
                {<NBAR><IN><NBAR>}  # Above, connected with in/of/etc...
        """
        chunker = nltk.RegexpParser(grammar)

        toks = nltk.regexp_tokenize(text, sentence_re)
        postoks = nltk.tag.pos_tag(toks)
        tree = chunker.parse(postoks)

        from nltk.corpus import stopwords
        stopwords = stopwords.words('english')


        def leaves(tree):
            """Finds NP (nounphrase) leaf nodes of a chunk tree."""
            for subtree in tree.subtrees(filter = lambda t: t.label()=='NP'):
                yield subtree.leaves()

        def normalise(word):
            """Normalises words to lowercase and stems and lemmatizes it."""
            word = word.lower()
            #word = stemmer.stem(word)
            word = lemmatizer.lemmatize(word)
            return word

        def non_normalise(word):
            """Normalises words to lowercase and stems and lemmatizes it."""
            #word = word.lower()
            #word = stemmer.stem(word)
            word = lemmatizer.lemmatize(word)
            return word

        def acceptable_word(word):
            """Checks conditions for acceptable word: length, stopword."""
            accepted = bool(2 <= len(word) <= 40
                            and word.lower() not in stopwords)
            return accepted


        def get_terms(tree):
            for leaf in leaves(tree):
                term = [ normalise(w) for w,t in leaf if acceptable_word(w) ]
                yield term


        def get_terms_non_normalise(tree):
            for leaf in leaves(tree):
                term = [ non_normalise(w) for w,t in leaf if acceptable_word(w) ]
                yield term


        terms = get_terms(tree)
        terms_non_normalise = get_terms_non_normalise(tree)

        for term in terms:
            line = ""
            for word in term:
                line = line + " " + word
                line = line.lstrip()
                phrases.append(line)
                #print(word)
            #print(term)

        for term in terms_non_normalise:
            line = ""
            for word in term:
                line = line + " " + word
                line = line.lstrip()
                phrases.append(line)

    except Exception as e:
        print("Error ")
        print(e)

    return jsonify(list(OrderedDict.fromkeys(phrases)))
if __name__=="__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(debug=True,host='0.0.0.0',port=port)