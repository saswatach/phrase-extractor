#   -*- coding: utf-8 -*-
from pybuilder.core import use_plugin, init, task, Project, dependents
from pathlib import Path
import shutil

# use_plugin('filter_resources')

use_plugin("python.core")
use_plugin("python.unittest")
use_plugin("python.flake8")
use_plugin("python.distutils")
use_plugin("python.install_dependencies")

use_plugin("exec")

name = "phrase-extractor"
default_task = "publish"
version = '0.0.1'

use_plugin("pypi:pybuilder_docker", version="0.3.9")
use_plugin("pypi:pybuilder_bumpversion", version="1.1.1")


@task(description="Copies the local pip.conf file to the Docker staging location.")
@dependents("compile_sources")
def copy_pip_conf(logger):
    """Before the Docker package task can run, we need to copy the local pip conf

    This is to allow the pip package within the Docker image to connect
    to Artifactory
    """
    pipconf_path = "/Library/Developer/CommandLineTools/Library/Frameworks/Python3.framework/Versions/3.8/pip.conf"
    docker_path = Path("src/main/docker")
    logger.info(f"Copying {pipconf_path} to {docker_path}")
    shutil.copy(pipconf_path, docker_path)

@init
def set_properties(project: Project):

    project.set_property("distutils_upload_repository_key", "ravn-snapshots" if "dev" in version else "ravn-releases")

    # Docker build
    project.set_property("docker_push_registry", "ravn.docker.imanage.com")

    # Build dependencies
    project.build_depends_on("bump2version")
    project.depends_on("kfp==1.8.2")
    project.depends_on('flask')
    project.depends_on('nltk')

    # project.get_property('filter_resources_glob').append('**/__init__.py')
    project.set_property("dir_source_main_python", "src/main/python/")

@task
def install_nltk_dep():
    import nltk
    nltk.download('punkt')
    nltk.download('stopwords')
    nltk.download('averaged_perceptron_tagger')
    nltk.download('wordnet')
    nltk.download('omw-1.4')